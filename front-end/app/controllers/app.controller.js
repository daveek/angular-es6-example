export default class AppController {

    constructor($scope) {
        this.$scope = $scope;

        $scope.$on('$stateChangeSuccess',
            (event, toState, toParams, fromState, fromParams) => this.stateChanged());
    }

    stateChanged (toState) {
        // State change event logic
    }
    
}

AppController.$inject = ['$scope'];