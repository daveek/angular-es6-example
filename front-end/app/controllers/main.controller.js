export default class MainController {

    constructor($scope) {
        this.modalOpen = false;

        $scope.$on('modalOpened', () =>  this.modalOpen = true);
        $scope.$on('modalClosed', () =>  this.modalOpen = false);
    }
    
    click (event) {
        //body click logic
    }
}

MainController.$inject = ['$scope'];